name := "ScalatronAkkaCommons"

version := "0.1"

organization := "kermitas"

scalaVersion := "2.10.1"

// collect all dependencies
retrieveManaged := true

// for debugging sbt problems
logLevel := Level.Debug

