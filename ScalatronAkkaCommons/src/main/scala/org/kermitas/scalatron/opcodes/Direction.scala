package main.scala.org.kermitas.scalatron.opcodes
{
  // ----

  trait Directions extends _root_.scala.Serializable
  {
    def getDirectionAsInt : Int
  }

    trait HorizontalDirections extends Directions

      object Left extends HorizontalDirections
      {
        override def getDirectionAsInt : Int = -1
      }

      object Right extends HorizontalDirections
      {
        override def getDirectionAsInt : Int = 1
      }

    trait VerticalDirections extends Directions

      object Up extends VerticalDirections
      {
        override def getDirectionAsInt : Int = -1
      }

      object Down extends VerticalDirections
      {
        override def getDirectionAsInt : Int = 1
      }

    object Stay extends HorizontalDirections with VerticalDirections
    {
      override def getDirectionAsInt : Int = 0
    }

  // ----

  object Direction
  {
    def getHorizontalDirection( horizontalDirectionAsInt : Int ) : Option[ HorizontalDirections ] =
    {
      horizontalDirectionAsInt match
      {
        case -1 => Some( Left )
        case 0 => Some( Stay )
        case 1 => Some( Right )
        case _ => None
      }
    }

    def getVerticalDirection( verticalDirectionAsInt : Int ) : Option[ VerticalDirections ] =
    {
      verticalDirectionAsInt match
      {
        case -1 => Some( Up )
        case 0 => Some( Stay )
        case 1 => Some( Down )
        case _ => None
      }
    }

    def getDirection( horizontalDirectionAsInt : Int , verticalDirectionAsInt : Int ) : Option[ Direction ] =
    {
      val horizontalDirectionOption = getHorizontalDirection( horizontalDirectionAsInt )
      val verticalDirectionOption = getVerticalDirection( verticalDirectionAsInt )

      if( horizontalDirectionOption.isEmpty ) None
      else
      if( verticalDirectionOption.isEmpty ) None
      else
      Some( new Direction( horizontalDirectionOption.get , verticalDirectionOption.get ) )
    }
  }

  // ----

  case class Direction( horizontalDirection : HorizontalDirections , verticalDirection : VerticalDirections ) extends _root_.scala.Serializable

  // ----
}