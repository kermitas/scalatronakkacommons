package main.scala.org.kermitas.scalatron.opcodes
{
  // ----

  trait CellOccupy extends _root_.scala.Serializable

    trait OwnerType extends CellOccupy
      trait MyOwn extends OwnerType
      trait EnemyOwn extends OwnerType
      trait GameOwn extends OwnerType

    trait MovementType extends CellOccupy
      trait Movable extends MovementType
      trait Static extends MovementType

    trait EatType extends CellOccupy
      trait CanBeEatenByMyMasterBot extends EatType
      trait CanBeEatenByMyMiniBot extends EatType

    trait ObstacleType extends CellOccupy
      trait HardObstacleType extends ObstacleType // next movement can produce bonk
        trait HardObstacleForMyMasterBot extends HardObstacleType
        trait HardObstacleForMyMiniBot extends HardObstacleType
      trait SoftObstacleType extends ObstacleType // next movement can destroy me (mini-bot) or decrease my energy (for example master-bot will step on Toxifera)
        trait WillHurtMyMasterBot extends SoftObstacleType
        trait WillHurtMyMiniBot extends SoftObstacleType

    trait LifeFormType extends CellOccupy
      trait Plant extends LifeFormType
      trait Beast extends LifeFormType
      trait BotType extends LifeFormType
        trait MasterBot extends BotType
        trait MiniBot extends BotType

  // ----

  object Unknown extends CellOccupy // ?
  object Empty extends GameOwn // _
  object Wall extends GameOwn with Static with HardObstacleForMyMasterBot with HardObstacleForMyMiniBot // W

  object MyMasterBot extends MyOwn with MasterBot with Movable // M
  object EnemyMasterBot extends EnemyOwn with MasterBot with Movable with HardObstacleForMyMasterBot with WillHurtMyMiniBot // m

  object MyMiniBot extends MyOwn with MiniBot with Movable with HardObstacleForMyMiniBot // S
  object EnemyMiniBot extends EnemyOwn with MiniBot with Movable with WillHurtMyMiniBot with CanBeEatenByMyMasterBot // s

  object Zugar extends GameOwn with Plant with Static with CanBeEatenByMyMasterBot with CanBeEatenByMyMiniBot // P
  object Toxifera extends GameOwn with Plant with Static with WillHurtMyMasterBot with WillHurtMyMiniBot // p

  object Fluppet extends GameOwn with Beast with Movable with CanBeEatenByMyMasterBot with CanBeEatenByMyMiniBot // B
  object Snorg extends GameOwn with Beast with Movable with WillHurtMyMasterBot with WillHurtMyMiniBot // b

  // ----
}