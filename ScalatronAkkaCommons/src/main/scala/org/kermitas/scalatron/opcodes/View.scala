package main.scala.org.kermitas.scalatron.opcodes
{
  // ---

  /*
    "?" cell whose content is occluded by a wall
    "_" empty cell
    "W" wall
    "M" Bot (=master; yours, always in the center unless seen by a slave)
    "m" Bot (=master; enemy, not you)
    "S" Mini-bot (=slave, yours)
    "s" Mini-bot (=slave; enemy's, not yours)
    "P" Zugar (=good plant, food)
    "p" Toxifera (=bad plant, poisonous)
    "B" Fluppet (=good beast, food)
    "b" Snorg (=bad beast, predator)
  */

  // ----

  object View
  {
    def apply( view : String ) : View =
    {
      val squareSide = calculateSquareSide( view.length )
      new View( createTwoDimensionalArray( view , squareSide ) , squareSide )
    }

    // ---

    protected def calculateSquareSide( l : Int ) : Int =
    {
      val squareSide = math.sqrt( l )

      if( squareSide != math.floor( squareSide ) ) throw new RuntimeException( "Square root (" + squareSide + ") of " + l + " (the view string length) is not integer" )

      squareSide.toInt
    }

    protected def createTwoDimensionalArray( view : String , squareSide : Int ) : Array[ Array[ CellOccupy ] ] =
    {
      val viewArray = new Array[ Array[ CellOccupy ] ]( squareSide )

      for ( y <- 0 until squareSide )
      {
        val arrayX = new Array[ CellOccupy ]( squareSide )
        viewArray( y ) = arrayX

        for ( x <- 0 until squareSide )
        {
          arrayX( x ) = view.charAt( y*squareSide + x ) match
          {
            case '?' => Unknown
            case '_' => Empty
            case 'W' => Wall
            case 'M' => MyMasterBot
            case 'm' => EnemyMasterBot
            case 'S' => MyMiniBot
            case 's' => EnemyMiniBot
            case 'P' => Zugar
            case 'p' => Toxifera
            case 'B' => Fluppet
            case 'b' => Snorg
            case unknown => throw new RuntimeException( "Unknown cell occupant '" + unknown + "' (at index " + ( y*squareSide + x ) + ") in view '" + view + "')" )
          }
        }
      }

      viewArray
    }
  }

  // ----

  case class View( array : Array[ Array[ CellOccupy ] ] , squareSide : Int ) extends _root_.scala.Serializable
  {
    def drawToString : String =
    {
      val newLine = System.getProperty( "line.separator" )

      val sb = new StringBuilder

      for ( y <- 0 until squareSide )
      {
        for ( x <- 0 until squareSide )
        {
          val c = array(y)(x) match
          {
            case Unknown => '?'
            case Empty => '_'
            case Wall => 'W'
            case MyMasterBot => 'M'
            case EnemyMasterBot => 'm'
            case MyMiniBot => 'S'
            case EnemyMiniBot => 's'
            case Zugar => 'P'
            case Toxifera => 'p'
            case Fluppet => 'B'
            case Snorg => 'b'
            case unknown => throw new RuntimeException( "Unknown cell occupant '" + unknown + "' (at position " + x + ":" + y + ")" )
          }

          sb.append( c )
        }

        sb.append( newLine )
      }

      sb.toString
    }
  }

  // ----
}