package main.scala.org.kermitas.scalatron.opcodes
{
  import java.awt.Point

  // ----------------------------------------

  trait Opcode extends _root_.scala.Serializable

  // ----------------------------------------

    trait IncomingOpcode extends Opcode

      case class Welcome(name: String , apocalypse: Int , round: Int , maxSlavesOption: Option[Int] ) extends IncomingOpcode // Scalatron 1.1.0.2 does not emit "maxSlaves : Int" inside 'Welcome'

      abstract class AbstractBotReact(val generation: Int , val name: String , val time: Int , val view: View , val energy: Int , val collisionOption: Option[Direction] , val slavesOption: Option[Int] , val customProperties: scala.collection.mutable.Map[String, String] ) extends IncomingOpcode

        case class MasterBotReact(override val generation: Int , override val name: String , override val time: Int , override val view: View , override val energy: Int , override val collisionOption: Option[Direction] , override val slavesOption: Option[Int] , override val customProperties: scala.collection.mutable.Map[String, String] ) extends AbstractBotReact(generation , name , time , view , energy , collisionOption , slavesOption , customProperties)

        case class MiniBotReact(override val generation: Int , override val name: String , override val time: Int ,override val  view: View , override val energy: Int , master: Point , override val collisionOption: Option[Direction] , override val slavesOption: Option[Int] , override val customProperties: scala.collection.mutable.Map[String, String] ) extends AbstractBotReact(generation , name , time , view , energy , collisionOption , slavesOption , customProperties)

      case class Goodbye(energy: Int , customProperties: scala.collection.mutable.Map[String, String] ) extends IncomingOpcode

  // ----------------------------------------

    trait OutgoingOpcode extends Opcode

      case class Move(direction: Direction , customPropertiesOption: Option[scala.collection.Map[String, String]] ) extends OutgoingOpcode

      case class Spawn(direction: Direction , name: String , energy: Int , customPropertiesOption: Option[scala.collection.Map[String, String]] ) extends OutgoingOpcode
      {
        require(energy >= 100, "Energy should be minimum 100")
      }

      case class Set(customProperties: scala.collection.Map[String, String] ) extends OutgoingOpcode

      case class Explode( size: Int ) extends OutgoingOpcode
      {
        require(size > 2 && size < 10, "Size should be not less than 3 and not bigger 9")
      }

      case class Say( text: String ) extends OutgoingOpcode
      {
        TextingOutgoingOpcodeTextValidator(text, 10)
      }

      case class Status( text: String ) extends OutgoingOpcode
      {
        TextingOutgoingOpcodeTextValidator(text, 20)
      }

      case class Log( text: String ) extends OutgoingOpcode
      {
        TextingOutgoingOpcodeTextValidator(text, Int.MaxValue)
      }

      case class MarkCell( position: Point , color: String ) extends OutgoingOpcode

      case class DrawLine( from: Point , to: Point , color: String ) extends OutgoingOpcode

  // ----------------------------------------

  object TextingOutgoingOpcodeTextValidator
  {
    def apply( text : String , maxLength : Int )
    {
      if (text.length > maxLength) throw new IllegalArgumentException ("Text length should be maximum " + maxLength + " characters long (now it has " + text.length + " in '" + text + "')")
      if (text.contains (",") || text.contains ("|") || text.contains ("=") || text.contains ("(") || text.contains (")") ) throw new IllegalArgumentException ("Text cannot contains ,|=()")
    }
  }

  // ----------------------------------------
}